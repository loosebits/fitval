﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FitnessResults
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FitnessResults))
        Me.HeightText = New System.Windows.Forms.TextBox()
        Me.WeightLabel = New System.Windows.Forms.Label()
        Me.BMILabel = New System.Windows.Forms.Label()
        Me.RestingHRLabel = New System.Windows.Forms.Label()
        Me.ExerciseHRLabel = New System.Windows.Forms.Label()
        Me.RecoveryHRLabel = New System.Windows.Forms.Label()
        Me.LungFuncLabel = New System.Windows.Forms.Label()
        Me.BloodPressureSystolicLabel = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Diastolic_Red = New System.Windows.Forms.TextBox()
        Me.Waist_Red = New System.Windows.Forms.TextBox()
        Me.Overall_Red = New System.Windows.Forms.TextBox()
        Me.Overall_Yellow = New System.Windows.Forms.TextBox()
        Me.Waist_Yellow = New System.Windows.Forms.TextBox()
        Me.Diastolic_Yellow = New System.Windows.Forms.TextBox()
        Me.Overall_Green = New System.Windows.Forms.TextBox()
        Me.Waist_Green = New System.Windows.Forms.TextBox()
        Me.Diastolic_Green = New System.Windows.Forms.TextBox()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.ClientInformationGroup = New System.Windows.Forms.GroupBox()
        Me.ClientIDLabel = New System.Windows.Forms.Label()
        Me.FirstNameLabel = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.MaleRadio = New System.Windows.Forms.RadioButton()
        Me.FemaleRadio = New System.Windows.Forms.RadioButton()
        Me.LastNameLabel = New System.Windows.Forms.Label()
        Me.GenderLabel = New System.Windows.Forms.Label()
        Me.ClientIDText = New System.Windows.Forms.TextBox()
        Me.FirstNameText = New System.Windows.Forms.TextBox()
        Me.BirthDateLabel = New System.Windows.Forms.Label()
        Me.LastNameText = New System.Windows.Forms.TextBox()
        Me.JobLabel = New System.Windows.Forms.Label()
        Me.JobText = New System.Windows.Forms.TextBox()
        Me.BirthDatePicker = New System.Windows.Forms.DateTimePicker()
        Me.PersonalOutcomeGroup = New System.Windows.Forms.GroupBox()
        Me.Outcome1Label = New System.Windows.Forms.Label()
        Me.Outcome2Label = New System.Windows.Forms.Label()
        Me.Outcome3Label = New System.Windows.Forms.Label()
        Me.Outcome1Text = New System.Windows.Forms.TextBox()
        Me.Outcome2Text = New System.Windows.Forms.TextBox()
        Me.Outcome3Text = New System.Windows.Forms.TextBox()
        Me.TestResultsGroup = New System.Windows.Forms.GroupBox()
        Me.OverallFitnessText = New System.Windows.Forms.TextBox()
        Me.WaistTextG = New System.Windows.Forms.TextBox()
        Me.WaistTextY = New System.Windows.Forms.TextBox()
        Me.WaistTextR = New System.Windows.Forms.TextBox()
        Me.DiastolicTextG = New System.Windows.Forms.TextBox()
        Me.DiastolicTextY = New System.Windows.Forms.TextBox()
        Me.DiastolicTextR = New System.Windows.Forms.TextBox()
        Me.SystolicTextG = New System.Windows.Forms.TextBox()
        Me.SystolicTextY = New System.Windows.Forms.TextBox()
        Me.SystolicTextR = New System.Windows.Forms.TextBox()
        Me.LungFuncTextG = New System.Windows.Forms.TextBox()
        Me.LungFuncTextY = New System.Windows.Forms.TextBox()
        Me.LungFuncTextR = New System.Windows.Forms.TextBox()
        Me.RecoveryHRTextG = New System.Windows.Forms.TextBox()
        Me.RecoveryHRTextY = New System.Windows.Forms.TextBox()
        Me.RecoveryHRTextR = New System.Windows.Forms.TextBox()
        Me.ExerciseHRTextG = New System.Windows.Forms.TextBox()
        Me.ExerciseHRTextY = New System.Windows.Forms.TextBox()
        Me.ExerciseHRTextR = New System.Windows.Forms.TextBox()
        Me.RestingHRTextG = New System.Windows.Forms.TextBox()
        Me.RestingHRTextY = New System.Windows.Forms.TextBox()
        Me.RestingHRTextR = New System.Windows.Forms.TextBox()
        Me.BMITextG = New System.Windows.Forms.TextBox()
        Me.BMITextY = New System.Windows.Forms.TextBox()
        Me.BMITextR = New System.Windows.Forms.TextBox()
        Me.WeightTextG = New System.Windows.Forms.TextBox()
        Me.WeightTextY = New System.Windows.Forms.TextBox()
        Me.WeightTextR = New System.Windows.Forms.TextBox()
        Me.OverallFitnessLabel = New System.Windows.Forms.Label()
        Me.AssessmentDateLabel = New System.Windows.Forms.Label()
        Me.AssessmentDatePicker = New System.Windows.Forms.DateTimePicker()
        Me.BloodPressureDiastolicLabel = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.WaistLabel = New System.Windows.Forms.Label()
        Me.BackButton = New System.Windows.Forms.Button()
        Me.Print = New System.Windows.Forms.Button()
        Me.PrintForm1 = New Microsoft.VisualBasic.PowerPacks.Printing.PrintForm(Me.components)
        Me.PrintDialog1 = New System.Windows.Forms.PrintDialog()
        Me.ClientInformationGroup.SuspendLayout()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.PersonalOutcomeGroup.SuspendLayout()
        Me.TestResultsGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'HeightText
        '
        Me.HeightText.Location = New System.Drawing.Point(135, 45)
        Me.HeightText.Name = "HeightText"
        Me.HeightText.Size = New System.Drawing.Size(421, 20)
        Me.HeightText.TabIndex = 2
        '
        'WeightLabel
        '
        Me.WeightLabel.AutoSize = True
        Me.WeightLabel.Location = New System.Drawing.Point(88, 74)
        Me.WeightLabel.Name = "WeightLabel"
        Me.WeightLabel.Size = New System.Drawing.Size(41, 13)
        Me.WeightLabel.TabIndex = 4
        Me.WeightLabel.Text = "Weight"
        '
        'BMILabel
        '
        Me.BMILabel.AutoSize = True
        Me.BMILabel.Location = New System.Drawing.Point(103, 100)
        Me.BMILabel.Name = "BMILabel"
        Me.BMILabel.Size = New System.Drawing.Size(26, 13)
        Me.BMILabel.TabIndex = 5
        Me.BMILabel.Text = "BMI"
        '
        'RestingHRLabel
        '
        Me.RestingHRLabel.AutoSize = True
        Me.RestingHRLabel.Location = New System.Drawing.Point(31, 126)
        Me.RestingHRLabel.Name = "RestingHRLabel"
        Me.RestingHRLabel.Size = New System.Drawing.Size(98, 13)
        Me.RestingHRLabel.TabIndex = 6
        Me.RestingHRLabel.Text = "Resting Heart Rate"
        '
        'ExerciseHRLabel
        '
        Me.ExerciseHRLabel.AutoSize = True
        Me.ExerciseHRLabel.Location = New System.Drawing.Point(27, 152)
        Me.ExerciseHRLabel.Name = "ExerciseHRLabel"
        Me.ExerciseHRLabel.Size = New System.Drawing.Size(102, 13)
        Me.ExerciseHRLabel.TabIndex = 7
        Me.ExerciseHRLabel.Text = "Exercise Heart Rate"
        '
        'RecoveryHRLabel
        '
        Me.RecoveryHRLabel.AutoSize = True
        Me.RecoveryHRLabel.Location = New System.Drawing.Point(21, 178)
        Me.RecoveryHRLabel.Name = "RecoveryHRLabel"
        Me.RecoveryHRLabel.Size = New System.Drawing.Size(108, 13)
        Me.RecoveryHRLabel.TabIndex = 8
        Me.RecoveryHRLabel.Text = "Recovery Heart Rate"
        '
        'LungFuncLabel
        '
        Me.LungFuncLabel.AutoSize = True
        Me.LungFuncLabel.Location = New System.Drawing.Point(54, 204)
        Me.LungFuncLabel.Name = "LungFuncLabel"
        Me.LungFuncLabel.Size = New System.Drawing.Size(75, 13)
        Me.LungFuncLabel.TabIndex = 9
        Me.LungFuncLabel.Text = "Lung Function"
        '
        'BloodPressureSystolicLabel
        '
        Me.BloodPressureSystolicLabel.AutoSize = True
        Me.BloodPressureSystolicLabel.Location = New System.Drawing.Point(6, 230)
        Me.BloodPressureSystolicLabel.Name = "BloodPressureSystolicLabel"
        Me.BloodPressureSystolicLabel.Size = New System.Drawing.Size(123, 13)
        Me.BloodPressureSystolicLabel.TabIndex = 10
        Me.BloodPressureSystolicLabel.Text = "Blood Pressure (Systolic)"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(27, 675)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 13)
        Me.Label8.TabIndex = 11
        Me.Label8.Text = "Blood Dia"
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(27, 718)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(34, 13)
        Me.Label9.TabIndex = 12
        Me.Label9.Text = "Waist"
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(24, 753)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(76, 13)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Overall Fitness"
        '
        'Diastolic_Red
        '
        Me.Diastolic_Red.Location = New System.Drawing.Point(145, 675)
        Me.Diastolic_Red.Name = "Diastolic_Red"
        Me.Diastolic_Red.Size = New System.Drawing.Size(100, 20)
        Me.Diastolic_Red.TabIndex = 19
        '
        'Waist_Red
        '
        Me.Waist_Red.Location = New System.Drawing.Point(145, 711)
        Me.Waist_Red.Name = "Waist_Red"
        Me.Waist_Red.Size = New System.Drawing.Size(100, 20)
        Me.Waist_Red.TabIndex = 20
        '
        'Overall_Red
        '
        Me.Overall_Red.Location = New System.Drawing.Point(145, 746)
        Me.Overall_Red.Name = "Overall_Red"
        Me.Overall_Red.Size = New System.Drawing.Size(100, 20)
        Me.Overall_Red.TabIndex = 21
        '
        'Overall_Yellow
        '
        Me.Overall_Yellow.Location = New System.Drawing.Point(296, 746)
        Me.Overall_Yellow.Name = "Overall_Yellow"
        Me.Overall_Yellow.Size = New System.Drawing.Size(100, 20)
        Me.Overall_Yellow.TabIndex = 31
        '
        'Waist_Yellow
        '
        Me.Waist_Yellow.Location = New System.Drawing.Point(296, 711)
        Me.Waist_Yellow.Name = "Waist_Yellow"
        Me.Waist_Yellow.Size = New System.Drawing.Size(100, 20)
        Me.Waist_Yellow.TabIndex = 30
        '
        'Diastolic_Yellow
        '
        Me.Diastolic_Yellow.Location = New System.Drawing.Point(296, 675)
        Me.Diastolic_Yellow.Name = "Diastolic_Yellow"
        Me.Diastolic_Yellow.Size = New System.Drawing.Size(100, 20)
        Me.Diastolic_Yellow.TabIndex = 29
        '
        'Overall_Green
        '
        Me.Overall_Green.Location = New System.Drawing.Point(453, 746)
        Me.Overall_Green.Name = "Overall_Green"
        Me.Overall_Green.Size = New System.Drawing.Size(100, 20)
        Me.Overall_Green.TabIndex = 41
        '
        'Waist_Green
        '
        Me.Waist_Green.Location = New System.Drawing.Point(453, 711)
        Me.Waist_Green.Name = "Waist_Green"
        Me.Waist_Green.Size = New System.Drawing.Size(100, 20)
        Me.Waist_Green.TabIndex = 40
        '
        'Diastolic_Green
        '
        Me.Diastolic_Green.Location = New System.Drawing.Point(453, 675)
        Me.Diastolic_Green.Name = "Diastolic_Green"
        Me.Diastolic_Green.Size = New System.Drawing.Size(100, 20)
        Me.Diastolic_Green.TabIndex = 39
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(596, 763)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 43
        Me.Button1.Text = "Print"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'ClientInformationGroup
        '
        Me.ClientInformationGroup.Controls.Add(Me.ClientIDLabel)
        Me.ClientInformationGroup.Controls.Add(Me.FirstNameLabel)
        Me.ClientInformationGroup.Controls.Add(Me.FlowLayoutPanel1)
        Me.ClientInformationGroup.Controls.Add(Me.LastNameLabel)
        Me.ClientInformationGroup.Controls.Add(Me.GenderLabel)
        Me.ClientInformationGroup.Controls.Add(Me.ClientIDText)
        Me.ClientInformationGroup.Controls.Add(Me.FirstNameText)
        Me.ClientInformationGroup.Controls.Add(Me.BirthDateLabel)
        Me.ClientInformationGroup.Controls.Add(Me.LastNameText)
        Me.ClientInformationGroup.Controls.Add(Me.JobLabel)
        Me.ClientInformationGroup.Controls.Add(Me.JobText)
        Me.ClientInformationGroup.Controls.Add(Me.BirthDatePicker)
        Me.ClientInformationGroup.Location = New System.Drawing.Point(12, 12)
        Me.ClientInformationGroup.Name = "ClientInformationGroup"
        Me.ClientInformationGroup.Size = New System.Drawing.Size(612, 117)
        Me.ClientInformationGroup.TabIndex = 104
        Me.ClientInformationGroup.TabStop = False
        Me.ClientInformationGroup.Text = "Client Information"
        '
        'ClientIDLabel
        '
        Me.ClientIDLabel.AutoSize = True
        Me.ClientIDLabel.Location = New System.Drawing.Point(82, 28)
        Me.ClientIDLabel.Name = "ClientIDLabel"
        Me.ClientIDLabel.Size = New System.Drawing.Size(47, 13)
        Me.ClientIDLabel.TabIndex = 70
        Me.ClientIDLabel.Text = "Client ID"
        '
        'FirstNameLabel
        '
        Me.FirstNameLabel.AutoSize = True
        Me.FirstNameLabel.Location = New System.Drawing.Point(72, 53)
        Me.FirstNameLabel.Name = "FirstNameLabel"
        Me.FirstNameLabel.Size = New System.Drawing.Size(57, 13)
        Me.FirstNameLabel.TabIndex = 71
        Me.FirstNameLabel.Text = "First Name"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.MaleRadio)
        Me.FlowLayoutPanel1.Controls.Add(Me.FemaleRadio)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(330, 76)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(122, 24)
        Me.FlowLayoutPanel1.TabIndex = 99
        '
        'MaleRadio
        '
        Me.MaleRadio.AutoSize = True
        Me.MaleRadio.Location = New System.Drawing.Point(3, 3)
        Me.MaleRadio.Name = "MaleRadio"
        Me.MaleRadio.Size = New System.Drawing.Size(48, 17)
        Me.MaleRadio.TabIndex = 6
        Me.MaleRadio.TabStop = True
        Me.MaleRadio.Text = "Male"
        Me.MaleRadio.UseVisualStyleBackColor = True
        '
        'FemaleRadio
        '
        Me.FemaleRadio.AutoSize = True
        Me.FemaleRadio.Location = New System.Drawing.Point(57, 3)
        Me.FemaleRadio.Name = "FemaleRadio"
        Me.FemaleRadio.Size = New System.Drawing.Size(59, 17)
        Me.FemaleRadio.TabIndex = 7
        Me.FemaleRadio.TabStop = True
        Me.FemaleRadio.Text = "Female"
        Me.FemaleRadio.UseVisualStyleBackColor = True
        '
        'LastNameLabel
        '
        Me.LastNameLabel.AutoSize = True
        Me.LastNameLabel.Location = New System.Drawing.Point(71, 79)
        Me.LastNameLabel.Name = "LastNameLabel"
        Me.LastNameLabel.Size = New System.Drawing.Size(58, 13)
        Me.LastNameLabel.TabIndex = 72
        Me.LastNameLabel.Text = "Last Name"
        '
        'GenderLabel
        '
        Me.GenderLabel.AutoSize = True
        Me.GenderLabel.Location = New System.Drawing.Point(282, 81)
        Me.GenderLabel.Name = "GenderLabel"
        Me.GenderLabel.Size = New System.Drawing.Size(42, 13)
        Me.GenderLabel.TabIndex = 98
        Me.GenderLabel.Text = "Gender"
        '
        'ClientIDText
        '
        Me.ClientIDText.Location = New System.Drawing.Point(135, 25)
        Me.ClientIDText.Name = "ClientIDText"
        Me.ClientIDText.Size = New System.Drawing.Size(129, 20)
        Me.ClientIDText.TabIndex = 1
        '
        'FirstNameText
        '
        Me.FirstNameText.Location = New System.Drawing.Point(135, 50)
        Me.FirstNameText.Name = "FirstNameText"
        Me.FirstNameText.Size = New System.Drawing.Size(129, 20)
        Me.FirstNameText.TabIndex = 2
        '
        'BirthDateLabel
        '
        Me.BirthDateLabel.AutoSize = True
        Me.BirthDateLabel.Location = New System.Drawing.Point(270, 53)
        Me.BirthDateLabel.Name = "BirthDateLabel"
        Me.BirthDateLabel.Size = New System.Drawing.Size(54, 13)
        Me.BirthDateLabel.TabIndex = 86
        Me.BirthDateLabel.Text = "Birth Date"
        '
        'LastNameText
        '
        Me.LastNameText.Location = New System.Drawing.Point(135, 76)
        Me.LastNameText.Name = "LastNameText"
        Me.LastNameText.Size = New System.Drawing.Size(129, 20)
        Me.LastNameText.TabIndex = 3
        '
        'JobLabel
        '
        Me.JobLabel.AutoSize = True
        Me.JobLabel.Location = New System.Drawing.Point(300, 28)
        Me.JobLabel.Name = "JobLabel"
        Me.JobLabel.Size = New System.Drawing.Size(24, 13)
        Me.JobLabel.TabIndex = 85
        Me.JobLabel.Text = "Job"
        '
        'JobText
        '
        Me.JobText.Location = New System.Drawing.Point(330, 25)
        Me.JobText.Name = "JobText"
        Me.JobText.Size = New System.Drawing.Size(226, 20)
        Me.JobText.TabIndex = 4
        '
        'BirthDatePicker
        '
        Me.BirthDatePicker.Location = New System.Drawing.Point(330, 50)
        Me.BirthDatePicker.Name = "BirthDatePicker"
        Me.BirthDatePicker.Size = New System.Drawing.Size(226, 20)
        Me.BirthDatePicker.TabIndex = 5
        '
        'PersonalOutcomeGroup
        '
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome1Label)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome2Label)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome3Label)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome1Text)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome2Text)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome3Text)
        Me.PersonalOutcomeGroup.Location = New System.Drawing.Point(12, 135)
        Me.PersonalOutcomeGroup.Name = "PersonalOutcomeGroup"
        Me.PersonalOutcomeGroup.Size = New System.Drawing.Size(612, 113)
        Me.PersonalOutcomeGroup.TabIndex = 103
        Me.PersonalOutcomeGroup.TabStop = False
        Me.PersonalOutcomeGroup.Text = "Personal Outcomes"
        '
        'Outcome1Label
        '
        Me.Outcome1Label.AutoSize = True
        Me.Outcome1Label.Location = New System.Drawing.Point(70, 27)
        Me.Outcome1Label.Name = "Outcome1Label"
        Me.Outcome1Label.Size = New System.Drawing.Size(59, 13)
        Me.Outcome1Label.TabIndex = 78
        Me.Outcome1Label.Text = "Outcome 1"
        '
        'Outcome2Label
        '
        Me.Outcome2Label.AutoSize = True
        Me.Outcome2Label.Location = New System.Drawing.Point(70, 53)
        Me.Outcome2Label.Name = "Outcome2Label"
        Me.Outcome2Label.Size = New System.Drawing.Size(59, 13)
        Me.Outcome2Label.TabIndex = 79
        Me.Outcome2Label.Text = "Outcome 2"
        '
        'Outcome3Label
        '
        Me.Outcome3Label.AutoSize = True
        Me.Outcome3Label.Location = New System.Drawing.Point(70, 79)
        Me.Outcome3Label.Name = "Outcome3Label"
        Me.Outcome3Label.Size = New System.Drawing.Size(59, 13)
        Me.Outcome3Label.TabIndex = 80
        Me.Outcome3Label.Text = "Outcome 3"
        '
        'Outcome1Text
        '
        Me.Outcome1Text.Location = New System.Drawing.Point(135, 24)
        Me.Outcome1Text.Name = "Outcome1Text"
        Me.Outcome1Text.Size = New System.Drawing.Size(421, 20)
        Me.Outcome1Text.TabIndex = 8
        '
        'Outcome2Text
        '
        Me.Outcome2Text.Location = New System.Drawing.Point(135, 50)
        Me.Outcome2Text.Name = "Outcome2Text"
        Me.Outcome2Text.Size = New System.Drawing.Size(421, 20)
        Me.Outcome2Text.TabIndex = 9
        '
        'Outcome3Text
        '
        Me.Outcome3Text.Location = New System.Drawing.Point(135, 76)
        Me.Outcome3Text.Name = "Outcome3Text"
        Me.Outcome3Text.Size = New System.Drawing.Size(421, 20)
        Me.Outcome3Text.TabIndex = 10
        '
        'TestResultsGroup
        '
        Me.TestResultsGroup.Controls.Add(Me.OverallFitnessText)
        Me.TestResultsGroup.Controls.Add(Me.WaistTextG)
        Me.TestResultsGroup.Controls.Add(Me.WaistTextY)
        Me.TestResultsGroup.Controls.Add(Me.WaistTextR)
        Me.TestResultsGroup.Controls.Add(Me.DiastolicTextG)
        Me.TestResultsGroup.Controls.Add(Me.DiastolicTextY)
        Me.TestResultsGroup.Controls.Add(Me.DiastolicTextR)
        Me.TestResultsGroup.Controls.Add(Me.SystolicTextG)
        Me.TestResultsGroup.Controls.Add(Me.SystolicTextY)
        Me.TestResultsGroup.Controls.Add(Me.SystolicTextR)
        Me.TestResultsGroup.Controls.Add(Me.LungFuncTextG)
        Me.TestResultsGroup.Controls.Add(Me.LungFuncTextY)
        Me.TestResultsGroup.Controls.Add(Me.LungFuncTextR)
        Me.TestResultsGroup.Controls.Add(Me.RecoveryHRTextG)
        Me.TestResultsGroup.Controls.Add(Me.RecoveryHRTextY)
        Me.TestResultsGroup.Controls.Add(Me.RecoveryHRTextR)
        Me.TestResultsGroup.Controls.Add(Me.ExerciseHRTextG)
        Me.TestResultsGroup.Controls.Add(Me.ExerciseHRTextY)
        Me.TestResultsGroup.Controls.Add(Me.ExerciseHRTextR)
        Me.TestResultsGroup.Controls.Add(Me.RestingHRTextG)
        Me.TestResultsGroup.Controls.Add(Me.RestingHRTextY)
        Me.TestResultsGroup.Controls.Add(Me.RestingHRTextR)
        Me.TestResultsGroup.Controls.Add(Me.BMITextG)
        Me.TestResultsGroup.Controls.Add(Me.BMITextY)
        Me.TestResultsGroup.Controls.Add(Me.BMITextR)
        Me.TestResultsGroup.Controls.Add(Me.WeightTextG)
        Me.TestResultsGroup.Controls.Add(Me.WeightTextY)
        Me.TestResultsGroup.Controls.Add(Me.WeightTextR)
        Me.TestResultsGroup.Controls.Add(Me.OverallFitnessLabel)
        Me.TestResultsGroup.Controls.Add(Me.AssessmentDateLabel)
        Me.TestResultsGroup.Controls.Add(Me.AssessmentDatePicker)
        Me.TestResultsGroup.Controls.Add(Me.BloodPressureDiastolicLabel)
        Me.TestResultsGroup.Controls.Add(Me.Label1)
        Me.TestResultsGroup.Controls.Add(Me.WaistLabel)
        Me.TestResultsGroup.Controls.Add(Me.HeightText)
        Me.TestResultsGroup.Controls.Add(Me.WeightLabel)
        Me.TestResultsGroup.Controls.Add(Me.BMILabel)
        Me.TestResultsGroup.Controls.Add(Me.RestingHRLabel)
        Me.TestResultsGroup.Controls.Add(Me.ExerciseHRLabel)
        Me.TestResultsGroup.Controls.Add(Me.RecoveryHRLabel)
        Me.TestResultsGroup.Controls.Add(Me.LungFuncLabel)
        Me.TestResultsGroup.Controls.Add(Me.BloodPressureSystolicLabel)
        Me.TestResultsGroup.Location = New System.Drawing.Point(12, 254)
        Me.TestResultsGroup.Name = "TestResultsGroup"
        Me.TestResultsGroup.Size = New System.Drawing.Size(612, 339)
        Me.TestResultsGroup.TabIndex = 105
        Me.TestResultsGroup.TabStop = False
        Me.TestResultsGroup.Text = "Test Results"
        '
        'OverallFitnessText
        '
        Me.OverallFitnessText.Location = New System.Drawing.Point(135, 305)
        Me.OverallFitnessText.Name = "OverallFitnessText"
        Me.OverallFitnessText.Size = New System.Drawing.Size(421, 20)
        Me.OverallFitnessText.TabIndex = 132
        '
        'WaistTextG
        '
        Me.WaistTextG.Location = New System.Drawing.Point(421, 279)
        Me.WaistTextG.Name = "WaistTextG"
        Me.WaistTextG.Size = New System.Drawing.Size(135, 20)
        Me.WaistTextG.TabIndex = 131
        '
        'WaistTextY
        '
        Me.WaistTextY.Location = New System.Drawing.Point(278, 279)
        Me.WaistTextY.Name = "WaistTextY"
        Me.WaistTextY.Size = New System.Drawing.Size(135, 20)
        Me.WaistTextY.TabIndex = 130
        '
        'WaistTextR
        '
        Me.WaistTextR.Location = New System.Drawing.Point(135, 279)
        Me.WaistTextR.Name = "WaistTextR"
        Me.WaistTextR.Size = New System.Drawing.Size(135, 20)
        Me.WaistTextR.TabIndex = 129
        '
        'DiastolicTextG
        '
        Me.DiastolicTextG.Location = New System.Drawing.Point(421, 253)
        Me.DiastolicTextG.Name = "DiastolicTextG"
        Me.DiastolicTextG.Size = New System.Drawing.Size(135, 20)
        Me.DiastolicTextG.TabIndex = 128
        '
        'DiastolicTextY
        '
        Me.DiastolicTextY.Location = New System.Drawing.Point(278, 253)
        Me.DiastolicTextY.Name = "DiastolicTextY"
        Me.DiastolicTextY.Size = New System.Drawing.Size(135, 20)
        Me.DiastolicTextY.TabIndex = 127
        '
        'DiastolicTextR
        '
        Me.DiastolicTextR.Location = New System.Drawing.Point(135, 253)
        Me.DiastolicTextR.Name = "DiastolicTextR"
        Me.DiastolicTextR.Size = New System.Drawing.Size(135, 20)
        Me.DiastolicTextR.TabIndex = 126
        '
        'SystolicTextG
        '
        Me.SystolicTextG.Location = New System.Drawing.Point(421, 227)
        Me.SystolicTextG.Name = "SystolicTextG"
        Me.SystolicTextG.Size = New System.Drawing.Size(135, 20)
        Me.SystolicTextG.TabIndex = 125
        '
        'SystolicTextY
        '
        Me.SystolicTextY.Location = New System.Drawing.Point(278, 227)
        Me.SystolicTextY.Name = "SystolicTextY"
        Me.SystolicTextY.Size = New System.Drawing.Size(135, 20)
        Me.SystolicTextY.TabIndex = 124
        '
        'SystolicTextR
        '
        Me.SystolicTextR.Location = New System.Drawing.Point(135, 227)
        Me.SystolicTextR.Name = "SystolicTextR"
        Me.SystolicTextR.Size = New System.Drawing.Size(135, 20)
        Me.SystolicTextR.TabIndex = 123
        '
        'LungFuncTextG
        '
        Me.LungFuncTextG.Location = New System.Drawing.Point(421, 201)
        Me.LungFuncTextG.Name = "LungFuncTextG"
        Me.LungFuncTextG.Size = New System.Drawing.Size(135, 20)
        Me.LungFuncTextG.TabIndex = 122
        '
        'LungFuncTextY
        '
        Me.LungFuncTextY.Location = New System.Drawing.Point(278, 201)
        Me.LungFuncTextY.Name = "LungFuncTextY"
        Me.LungFuncTextY.Size = New System.Drawing.Size(135, 20)
        Me.LungFuncTextY.TabIndex = 121
        '
        'LungFuncTextR
        '
        Me.LungFuncTextR.Location = New System.Drawing.Point(135, 201)
        Me.LungFuncTextR.Name = "LungFuncTextR"
        Me.LungFuncTextR.Size = New System.Drawing.Size(135, 20)
        Me.LungFuncTextR.TabIndex = 120
        '
        'RecoveryHRTextG
        '
        Me.RecoveryHRTextG.Location = New System.Drawing.Point(421, 175)
        Me.RecoveryHRTextG.Name = "RecoveryHRTextG"
        Me.RecoveryHRTextG.Size = New System.Drawing.Size(135, 20)
        Me.RecoveryHRTextG.TabIndex = 119
        '
        'RecoveryHRTextY
        '
        Me.RecoveryHRTextY.Location = New System.Drawing.Point(278, 175)
        Me.RecoveryHRTextY.Name = "RecoveryHRTextY"
        Me.RecoveryHRTextY.Size = New System.Drawing.Size(135, 20)
        Me.RecoveryHRTextY.TabIndex = 118
        '
        'RecoveryHRTextR
        '
        Me.RecoveryHRTextR.Location = New System.Drawing.Point(135, 175)
        Me.RecoveryHRTextR.Name = "RecoveryHRTextR"
        Me.RecoveryHRTextR.Size = New System.Drawing.Size(135, 20)
        Me.RecoveryHRTextR.TabIndex = 117
        '
        'ExerciseHRTextG
        '
        Me.ExerciseHRTextG.Location = New System.Drawing.Point(421, 149)
        Me.ExerciseHRTextG.Name = "ExerciseHRTextG"
        Me.ExerciseHRTextG.Size = New System.Drawing.Size(135, 20)
        Me.ExerciseHRTextG.TabIndex = 116
        '
        'ExerciseHRTextY
        '
        Me.ExerciseHRTextY.Location = New System.Drawing.Point(278, 149)
        Me.ExerciseHRTextY.Name = "ExerciseHRTextY"
        Me.ExerciseHRTextY.Size = New System.Drawing.Size(135, 20)
        Me.ExerciseHRTextY.TabIndex = 115
        '
        'ExerciseHRTextR
        '
        Me.ExerciseHRTextR.Location = New System.Drawing.Point(135, 149)
        Me.ExerciseHRTextR.Name = "ExerciseHRTextR"
        Me.ExerciseHRTextR.Size = New System.Drawing.Size(135, 20)
        Me.ExerciseHRTextR.TabIndex = 114
        '
        'RestingHRTextG
        '
        Me.RestingHRTextG.Location = New System.Drawing.Point(421, 123)
        Me.RestingHRTextG.Name = "RestingHRTextG"
        Me.RestingHRTextG.Size = New System.Drawing.Size(135, 20)
        Me.RestingHRTextG.TabIndex = 113
        '
        'RestingHRTextY
        '
        Me.RestingHRTextY.Location = New System.Drawing.Point(278, 123)
        Me.RestingHRTextY.Name = "RestingHRTextY"
        Me.RestingHRTextY.Size = New System.Drawing.Size(135, 20)
        Me.RestingHRTextY.TabIndex = 112
        '
        'RestingHRTextR
        '
        Me.RestingHRTextR.Location = New System.Drawing.Point(135, 123)
        Me.RestingHRTextR.Name = "RestingHRTextR"
        Me.RestingHRTextR.Size = New System.Drawing.Size(135, 20)
        Me.RestingHRTextR.TabIndex = 111
        '
        'BMITextG
        '
        Me.BMITextG.Location = New System.Drawing.Point(421, 97)
        Me.BMITextG.Name = "BMITextG"
        Me.BMITextG.Size = New System.Drawing.Size(135, 20)
        Me.BMITextG.TabIndex = 110
        '
        'BMITextY
        '
        Me.BMITextY.Location = New System.Drawing.Point(278, 97)
        Me.BMITextY.Name = "BMITextY"
        Me.BMITextY.Size = New System.Drawing.Size(135, 20)
        Me.BMITextY.TabIndex = 109
        '
        'BMITextR
        '
        Me.BMITextR.Location = New System.Drawing.Point(135, 97)
        Me.BMITextR.Name = "BMITextR"
        Me.BMITextR.Size = New System.Drawing.Size(135, 20)
        Me.BMITextR.TabIndex = 108
        '
        'WeightTextG
        '
        Me.WeightTextG.Location = New System.Drawing.Point(421, 71)
        Me.WeightTextG.Name = "WeightTextG"
        Me.WeightTextG.Size = New System.Drawing.Size(135, 20)
        Me.WeightTextG.TabIndex = 107
        '
        'WeightTextY
        '
        Me.WeightTextY.Location = New System.Drawing.Point(278, 71)
        Me.WeightTextY.Name = "WeightTextY"
        Me.WeightTextY.Size = New System.Drawing.Size(135, 20)
        Me.WeightTextY.TabIndex = 106
        '
        'WeightTextR
        '
        Me.WeightTextR.Location = New System.Drawing.Point(135, 71)
        Me.WeightTextR.Name = "WeightTextR"
        Me.WeightTextR.Size = New System.Drawing.Size(135, 20)
        Me.WeightTextR.TabIndex = 105
        '
        'OverallFitnessLabel
        '
        Me.OverallFitnessLabel.AutoSize = True
        Me.OverallFitnessLabel.Location = New System.Drawing.Point(53, 308)
        Me.OverallFitnessLabel.Name = "OverallFitnessLabel"
        Me.OverallFitnessLabel.Size = New System.Drawing.Size(76, 13)
        Me.OverallFitnessLabel.TabIndex = 102
        Me.OverallFitnessLabel.Text = "Overall Fitness"
        '
        'AssessmentDateLabel
        '
        Me.AssessmentDateLabel.AutoSize = True
        Me.AssessmentDateLabel.Location = New System.Drawing.Point(40, 22)
        Me.AssessmentDateLabel.Name = "AssessmentDateLabel"
        Me.AssessmentDateLabel.Size = New System.Drawing.Size(89, 13)
        Me.AssessmentDateLabel.TabIndex = 101
        Me.AssessmentDateLabel.Text = "Assessment Date"
        '
        'AssessmentDatePicker
        '
        Me.AssessmentDatePicker.Location = New System.Drawing.Point(135, 19)
        Me.AssessmentDatePicker.Name = "AssessmentDatePicker"
        Me.AssessmentDatePicker.Size = New System.Drawing.Size(421, 20)
        Me.AssessmentDatePicker.TabIndex = 100
        '
        'BloodPressureDiastolicLabel
        '
        Me.BloodPressureDiastolicLabel.AutoSize = True
        Me.BloodPressureDiastolicLabel.Location = New System.Drawing.Point(2, 256)
        Me.BloodPressureDiastolicLabel.Name = "BloodPressureDiastolicLabel"
        Me.BloodPressureDiastolicLabel.Size = New System.Drawing.Size(127, 13)
        Me.BloodPressureDiastolicLabel.TabIndex = 32
        Me.BloodPressureDiastolicLabel.Text = "Blood Pressure (Diastolic)"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(91, 48)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(38, 13)
        Me.Label1.TabIndex = 29
        Me.Label1.Text = "Height"
        '
        'WaistLabel
        '
        Me.WaistLabel.AutoSize = True
        Me.WaistLabel.Location = New System.Drawing.Point(95, 282)
        Me.WaistLabel.Name = "WaistLabel"
        Me.WaistLabel.Size = New System.Drawing.Size(34, 13)
        Me.WaistLabel.TabIndex = 27
        Me.WaistLabel.Text = "Waist"
        '
        'BackButton
        '
        Me.BackButton.Location = New System.Drawing.Point(549, 599)
        Me.BackButton.Name = "BackButton"
        Me.BackButton.Size = New System.Drawing.Size(75, 23)
        Me.BackButton.TabIndex = 106
        Me.BackButton.Text = "Back"
        Me.BackButton.UseVisualStyleBackColor = True
        '
        'Print
        '
        Me.Print.Location = New System.Drawing.Point(468, 599)
        Me.Print.Name = "Print"
        Me.Print.Size = New System.Drawing.Size(75, 23)
        Me.Print.TabIndex = 107
        Me.Print.Text = "Print"
        Me.Print.UseVisualStyleBackColor = True
        '
        'PrintForm1
        '
        Me.PrintForm1.Form = Me
        Me.PrintForm1.PrintAction = System.Drawing.Printing.PrintAction.PrintToPreview
        Me.PrintForm1.PrinterSettings = CType(resources.GetObject("PrintForm1.PrinterSettings"), System.Drawing.Printing.PrinterSettings)
        Me.PrintForm1.PrintFileName = Nothing
        '
        'PrintDialog1
        '
        Me.PrintDialog1.UseEXDialog = True
        '
        'FitnessResults
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PowderBlue
        Me.ClientSize = New System.Drawing.Size(651, 646)
        Me.Controls.Add(Me.Print)
        Me.Controls.Add(Me.BackButton)
        Me.Controls.Add(Me.TestResultsGroup)
        Me.Controls.Add(Me.ClientInformationGroup)
        Me.Controls.Add(Me.PersonalOutcomeGroup)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.Overall_Green)
        Me.Controls.Add(Me.Waist_Green)
        Me.Controls.Add(Me.Diastolic_Green)
        Me.Controls.Add(Me.Overall_Yellow)
        Me.Controls.Add(Me.Waist_Yellow)
        Me.Controls.Add(Me.Diastolic_Yellow)
        Me.Controls.Add(Me.Overall_Red)
        Me.Controls.Add(Me.Waist_Red)
        Me.Controls.Add(Me.Diastolic_Red)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Name = "FitnessResults"
        Me.Text = "Results"
        Me.ClientInformationGroup.ResumeLayout(False)
        Me.ClientInformationGroup.PerformLayout()
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.PersonalOutcomeGroup.ResumeLayout(False)
        Me.PersonalOutcomeGroup.PerformLayout()
        Me.TestResultsGroup.ResumeLayout(False)
        Me.TestResultsGroup.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents HeightText As TextBox
    Friend WithEvents WeightLabel As Label
    Friend WithEvents BMILabel As Label
    Friend WithEvents RestingHRLabel As Label
    Friend WithEvents ExerciseHRLabel As Label
    Friend WithEvents RecoveryHRLabel As Label
    Friend WithEvents LungFuncLabel As Label
    Friend WithEvents BloodPressureSystolicLabel As Label
    Friend WithEvents Label8 As Label
    Friend WithEvents Label9 As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents Diastolic_Red As TextBox
    Friend WithEvents Waist_Red As TextBox
    Friend WithEvents Overall_Red As TextBox
    Friend WithEvents Overall_Yellow As TextBox
    Friend WithEvents Waist_Yellow As TextBox
    Friend WithEvents Diastolic_Yellow As TextBox
    Friend WithEvents Overall_Green As TextBox
    Friend WithEvents Waist_Green As TextBox
    Friend WithEvents Diastolic_Green As TextBox
    Friend WithEvents Button1 As Button
    Friend WithEvents ClientInformationGroup As GroupBox
    Friend WithEvents ClientIDLabel As Label
    Friend WithEvents FirstNameLabel As Label
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents MaleRadio As RadioButton
    Friend WithEvents FemaleRadio As RadioButton
    Friend WithEvents LastNameLabel As Label
    Friend WithEvents GenderLabel As Label
    Friend WithEvents ClientIDText As TextBox
    Friend WithEvents FirstNameText As TextBox
    Friend WithEvents BirthDateLabel As Label
    Friend WithEvents LastNameText As TextBox
    Friend WithEvents JobLabel As Label
    Friend WithEvents JobText As TextBox
    Friend WithEvents BirthDatePicker As DateTimePicker
    Friend WithEvents PersonalOutcomeGroup As GroupBox
    Friend WithEvents Outcome1Label As Label
    Friend WithEvents Outcome2Label As Label
    Friend WithEvents Outcome3Label As Label
    Friend WithEvents Outcome1Text As TextBox
    Friend WithEvents Outcome2Text As TextBox
    Friend WithEvents Outcome3Text As TextBox
    Friend WithEvents TestResultsGroup As GroupBox
    Friend WithEvents WaistLabel As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents OverallFitnessLabel As Label
    Friend WithEvents AssessmentDateLabel As Label
    Friend WithEvents AssessmentDatePicker As DateTimePicker
    Friend WithEvents BloodPressureDiastolicLabel As Label
    Friend WithEvents OverallFitnessText As TextBox
    Friend WithEvents WaistTextG As TextBox
    Friend WithEvents WaistTextY As TextBox
    Friend WithEvents WaistTextR As TextBox
    Friend WithEvents DiastolicTextG As TextBox
    Friend WithEvents DiastolicTextY As TextBox
    Friend WithEvents DiastolicTextR As TextBox
    Friend WithEvents SystolicTextG As TextBox
    Friend WithEvents SystolicTextY As TextBox
    Friend WithEvents SystolicTextR As TextBox
    Friend WithEvents LungFuncTextG As TextBox
    Friend WithEvents LungFuncTextY As TextBox
    Friend WithEvents LungFuncTextR As TextBox
    Friend WithEvents RecoveryHRTextG As TextBox
    Friend WithEvents RecoveryHRTextY As TextBox
    Friend WithEvents RecoveryHRTextR As TextBox
    Friend WithEvents ExerciseHRTextG As TextBox
    Friend WithEvents ExerciseHRTextY As TextBox
    Friend WithEvents ExerciseHRTextR As TextBox
    Friend WithEvents RestingHRTextG As TextBox
    Friend WithEvents RestingHRTextY As TextBox
    Friend WithEvents RestingHRTextR As TextBox
    Friend WithEvents BMITextG As TextBox
    Friend WithEvents BMITextY As TextBox
    Friend WithEvents BMITextR As TextBox
    Friend WithEvents WeightTextG As TextBox
    Friend WithEvents WeightTextY As TextBox
    Friend WithEvents WeightTextR As TextBox
    Friend WithEvents BackButton As Button
    Friend WithEvents Print As Button
    Friend WithEvents PrintForm1 As PowerPacks.Printing.PrintForm
    Friend WithEvents PrintDialog1 As PrintDialog
End Class
