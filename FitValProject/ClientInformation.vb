﻿' FitVal Version 1.0
' Code written by: Ben Reich
' Initial Design Layout: Carlos Cerna

Imports System.Data.SQLite
Imports System.Text.RegularExpressions

Public Class ClientInformation
    Dim EnteredClientID As New Integer

    Dim FirstName As String
    Dim LastName As String
    Dim Job As String
    Dim Outcome_1 As String
    Dim Outcome_2 As String
    Dim Outcome_3 As String

    Public Sub New(Optional ByVal PassClientID As Integer = 0)
        InitializeComponent()
        ' If the client ID has been passed through, prepopulate the client information
        If PassClientID > 0 Then
            EnteredClientID = PassClientID
            ' Standardise the connection string from the Opening Form
            Using SQLConn As New SQLiteConnection(OpeningForm.connection)
                SQLConn.Open()
                Dim selectCommand = SQLConn.CreateCommand()
                selectCommand.CommandText = "SELECT * FROM Clients WHERE client_id = '" & EnteredClientID & "'"
                Using reader = selectCommand.ExecuteReader()
                    If reader.HasRows = True Then
                        While (reader.Read())
                            ' The client exists, perform some data preparation to conform to the form controls
                            ClientIDText.Text = EnteredClientID
                            ClientIDText.Enabled = False
                            Dim BirthDate = reader.GetString(2).Split("/")
                            BirthDatePicker.Value = New Date(BirthDate(2), BirthDate(1), BirthDate(0))
                            FirstName = reader.GetString(7)
                            FirstNameText.Text = FirstName
                            LastName = reader.GetString(8)
                            LastNameText.Text = LastName
                            Job = reader.GetString(6)
                            JobText.Text = Job
                            Dim gender = reader.GetInt32(9)
                            If (gender = 1) Then
                                MaleRadio.Checked = True
                            Else
                                FemaleRadio.Checked = True
                            End If
                            Outcome_1 = reader.GetString(10)
                            Outcome1Text.Text = Outcome_1
                            Outcome_2 = reader.GetString(11)
                            Outcome2Text.Text = Outcome_2
                            Outcome_3 = reader.GetString(12)
                            Outcome3Text.Text = Outcome_3
                        End While
                    End If
                End Using
                SQLConn.Close()
            End Using
        Else
            ' No client exists, don't show the ID but don't allow it to be entered
            ClientIDText.Text = "Generated on Submit"
            ClientIDText.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles NextButton.Click
        Using SQLConn As New SQLiteConnection(OpeningForm.connection)
            SQLConn.Open()
            Dim selectCommand = SQLConn.CreateCommand()
            ' If the client ID wasn't entered, utilise an INSERT SQL statement
            If ClientIDText.Text = "Generated on Submit" Then
                Dim FirstName, LastName, Job, Outcome1, Outcome2, Outcome3 As String
                Dim Gender As Integer
                ' Regex to strip all non alpha-numeric chars
                FirstName = Regex.Replace(FirstNameText.Text, "([^a-zA-Z\s])", String.Empty)
                LastName = Regex.Replace(LastNameText.Text, "([^a-zA-Z\s])", String.Empty)
                Job = Regex.Replace(JobText.Text, "([^a-zA-Z\s])", String.Empty)
                Gender = If(MaleRadio.Checked = True, 1, 2)
                Outcome1 = Regex.Replace(Outcome1Text.Text, "([^a-zA-Z0-9\s])", String.Empty)
                Outcome2 = Regex.Replace(Outcome2Text.Text, "([^a-zA-Z0-9\s])", String.Empty)
                Outcome3 = Regex.Replace(Outcome3Text.Text, "([^a-zA-Z0-9\s])", String.Empty)
                ' Construct the INSERT statement
                selectCommand.CommandText = "INSERT INTO Clients (name, birth_date, first_name, last_name, job, gender, poutcome_1, poutcome_2, poutcome_3) VALUES ( "
                selectCommand.CommandText += "'" & FirstName & " " & LastName & "', "
                selectCommand.CommandText += "'" & BirthDatePicker.Value.Day & "/" & BirthDatePicker.Value.Month & "/" & BirthDatePicker.Value.Year & "', "
                selectCommand.CommandText += "'" & FirstName & "', "
                selectCommand.CommandText += "'" & LastName & "', "
                selectCommand.CommandText += "'" & Job & "', "
                selectCommand.CommandText += "'" & Gender & "', "
                selectCommand.CommandText += "'" & Outcome1 & "', "
                selectCommand.CommandText += "'" & Outcome2 & "', "
                selectCommand.CommandText += "'" & Outcome3 & "')"
                selectCommand.ExecuteNonQuery()
                ' These next few lines, query the DB for the last inserted row ID
                ' Allows us to get the client ID to pass to the next form
                selectCommand.CommandText = "SELECT last_insert_rowid()"
                Using reader = selectCommand.ExecuteReader()
                    If reader.HasRows() Then
                        While (reader.Read())
                            ' Basically take the Client ID from the database and open the next form
                            Dim FitVal As New FitnessEvaluation(reader.GetInt32(0))
                            FitVal.Show()
                            Me.Hide()
                        End While
                    End If
                End Using
                ' The Client ID exists, utlise an UPDATE SQL statement to avoid reinserting clients
            ElseIf ClientIDText.Text.Length > 0 Then
                Dim FirstName, LastName, Job, Outcome1, Outcome2, Outcome3 As String
                Dim Gender As Integer
                FirstName = Regex.Replace(FirstNameText.Text, "([^a-zA-Z\s])", String.Empty)
                LastName = Regex.Replace(LastNameText.Text, "([^a-zA-Z\s])", String.Empty)
                Job = Regex.Replace(JobText.Text, "([^a-zA-Z\s])", String.Empty)
                Gender = If(MaleRadio.Checked = True, 1, 2)
                Outcome1 = Regex.Replace(Outcome1Text.Text, "([^a-zA-Z0-9\s])", String.Empty)
                Outcome2 = Regex.Replace(Outcome2Text.Text, "([^a-zA-Z0-9\s])", String.Empty)
                Outcome3 = Regex.Replace(Outcome3Text.Text, "([^a-zA-Z0-9\s])", String.Empty)
                selectCommand.CommandText = "UPDATE Clients Set "
                selectCommand.CommandText += "name = '" & FirstName & " " & LastName & "', "
                selectCommand.CommandText += "first_name = '" & FirstName & "', "
                selectCommand.CommandText += "last_name = '" & LastName & "', "
                selectCommand.CommandText += "job = '" & Job & "', "
                selectCommand.CommandText += "birth_date = '" & BirthDatePicker.Value.Day & "/" & BirthDatePicker.Value.Month & "/" & BirthDatePicker.Value.Year & "', "
                selectCommand.CommandText += "poutcome_1 = '" & Outcome1 & "', "
                selectCommand.CommandText += "poutcome_2 = '" & Outcome2 & "', "
                selectCommand.CommandText += "poutcome_3 = '" & Outcome3 & "', "
                selectCommand.CommandText += "gender = '" & Gender & "' "
                selectCommand.CommandText += "WHERE client_id = " & EnteredClientID
                selectCommand.ExecuteNonQuery()
                ' As the client ID exists, we don't have to get it because it was passed through on instatiation
                Dim FitVal As New FitnessEvaluation(EnteredClientID)
                FitVal.Show()
                Me.Hide()
            End If
            SQLConn.Close()
        End Using
    End Sub

    Private Sub ClientInformation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
    End Sub
End Class