﻿' FitVal Version 1.0
' Code written by: Ben Reich
' Initial Design Layout: Carlos Cerna

Imports System.Data.SQLite

Public Class FitnessEvaluation
    Dim ClientID As Integer

    Public Sub New(Optional ByVal PassClientID As Integer = 0)
        InitializeComponent()
        If PassClientID > 0 Then
            Me.ClientID = PassClientID
        End If
    End Sub

    Private Sub ViewResults_Click(sender As Object, e As EventArgs) Handles ViewResults.Click
        Dim Height, Weight, Waist, BMI As Double
        Dim RestingHR, ExerciseHR, RecoverHR, LungFunc, Systolic, Diastolic As Integer
        Dim errString, errStart As String
        errStart = "Invalid inputs for the following fields" & vbCrLf
        errString = errStart
        Try
            ' Basically use a Try Catch statement to attempt to push the data into
            ' it's associated variable, if it fails catch the error and end
            If HeightText.Text.Length > 0 Then Height = HeightText.Text
            errString += If(HeightText.Text.Length > 0, "", "- Height: No input" & vbCrLf)
            ' These check if anything was entered into the text field to begin with
            If WeightText.Text.Length > 0 Then Weight = WeightText.Text
            errString += If(WeightText.Text.Length > 0, "", "- Weight: No input" & vbCrLf)

            If RestingHRText.Text.Length > 0 Then RestingHR = RestingHRText.Text
            errString += If(RestingHRText.Text.Length > 0, "", "- Resting Heart Rate: No input" & vbCrLf)

            If ExerciseHRText.Text.Length > 0 Then ExerciseHR = ExerciseHRText.Text
            errString += If(ExerciseHRText.Text.Length > 0, "", "- Exercise Heart Rate: No input" & vbCrLf)

            If RecoverHRText.Text.Length > 0 Then RecoverHR = RecoverHRText.Text
            errString += If(RecoverHRText.Text.Length > 0, "", "- Recover Heart Rate: No input" & vbCrLf)

            If LungFuncText.Text.Length > 0 Then LungFunc = LungFuncText.Text
            errString += If(LungFuncText.Text.Length > 0, "", "- Lung Function: No input" & vbCrLf)

            If SystolicText.Text.Length > 0 Then Systolic = SystolicText.Text
            errString += If(SystolicText.Text.Length > 0, "", "- Lung Function: No input" & vbCrLf)

            If DiastolicText.Text.Length > 0 Then Diastolic = DiastolicText.Text
            errString += If(DiastolicText.Text.Length > 0, "", "- Lung Function: No input" & vbCrLf)

            If WaistText.Text.Length > 0 Then Waist = WaistText.Text
            errString += If(WaistText.Text.Length > 0, "", "- Lung Function: No input" & vbCrLf)
        Catch err As System.InvalidCastException
            errString += "- Invalid data in one input" & vbCrLf
        End Try
        If errString.Length > errStart.Length Then
            MsgBox(errString, MsgBoxStyle.Critical, "Errors Have Occurred")
        Else
            ' Calculate the BMI using Weight / Height^2 then convert it to a readable format
            BMI = Math.Round((Weight / Math.Pow(Height, 2)) * 10000, 2)
            Using SQLConn As New SQLiteConnection(OpeningForm.connection)
                SQLConn.Open()
                Dim selectCommand = SQLConn.CreateCommand()
                selectCommand.CommandText = "SELECT * FROM ClientData WHERE assessment_date = '" & AssessmentDate.Value.ToString("yyyy/MM/dd HH:mm:ss") & "' AND client_id = " & Me.ClientID.ToString()
                Using reader = selectCommand.ExecuteReader()
                    ' If the assessment already exists, update the values that were provided
                    If reader.HasRows() Then
                        Dim newSelectCommand = SQLConn.CreateCommand()
                        newSelectCommand.CommandText = "UPDATE ClientData SET height = '" & Height & "',
                                                                           weight = '" & Weight & "',
                                                                           bmi = '" & BMI & "',
                                                                           rest_hr = '" & RestingHR & "',
                                                                           max_hr = '" & ExerciseHR & "',
                                                                           rec_time = '" & RecoverHR & "',
                                                                           lung_func = '" & LungFunc & "',
                                                                           blood_press_sys = '" & Systolic & "',
                                                                           blood_press_dia = '" & Diastolic & "',
                                                                           waist = '" & Waist & "' WHERE client_id = '" & Me.ClientID & "' AND assessment_date = '" & AssessmentDate.Value.ToString("yyyy/MM/dd HH:mm:ss") & "'"
                        newSelectCommand.ExecuteNonQuery()
                        newSelectCommand.CommandText = "SELECT data_id FROM ClientData WHERE client_id = '" & Me.ClientID & "' AND assessment_date = '" & AssessmentDate.Value.ToString("yyyy/MM/dd HH:mm:ss") & "'"
                        Using reader_insert = newSelectCommand.ExecuteReader()
                            If reader_insert.HasRows() Then
                                While (reader_insert.Read())
                                    ' Pass that assessment ID onto the next form to show results
                                    Dim FitnessResults As New FitnessResults(reader_insert.GetInt32(0))
                                    FitnessResults.Show()
                                    Me.Hide()
                                End While
                            End If
                        End Using

                    Else
                        ' No assessment completed at this time, so treat it as new and insert it
                        Dim newSelectCommand = SQLConn.CreateCommand()
                        newSelectCommand.CommandText = "INSERT INTO ClientData ( client_id, assessment_date, height, weight, bmi, rest_hr, max_hr, rec_time, lung_func, blood_press_sys, blood_press_dia, waist )"
                        newSelectCommand.CommandText += " VALUES ("
                        ' client_id
                        newSelectCommand.CommandText += ClientID.ToString()
                        ' assessment_date
                        newSelectCommand.CommandText += ",'" & DateTime.Now.ToString("yyyy/MM/dd HH:mm:ss") & "'"
                        ' height
                        newSelectCommand.CommandText += ",'" & Height & "'"
                        ' weight
                        newSelectCommand.CommandText += ",'" & Weight & "'"
                        ' bmi
                        newSelectCommand.CommandText += ",'" & BMI & "'"
                        ' rest_hr
                        newSelectCommand.CommandText += ",'" & RestingHR & "'"
                        ' max_hr
                        newSelectCommand.CommandText += ",'" & ExerciseHR & "'"
                        ' rec_time
                        newSelectCommand.CommandText += ",'" & RecoverHR & "'"
                        ' lung_func
                        newSelectCommand.CommandText += ",'" & LungFunc & "'"
                        ' blood_press_sys
                        newSelectCommand.CommandText += "," & Systolic
                        ' blood_press_dia
                        newSelectCommand.CommandText += "," & Diastolic
                        ' waist
                        newSelectCommand.CommandText += ",'" & Waist & "');"
                        newSelectCommand.ExecuteNonQuery()
                        newSelectCommand.CommandText = "SELECT last_insert_rowid()"
                        Using reader_insert = newSelectCommand.ExecuteReader()
                            If reader_insert.HasRows() Then
                                While (reader_insert.Read())
                                    ' Select the last inserted row and pass that data onto the next form
                                    Dim FitnessResults As New FitnessResults(reader_insert.GetInt32(0))
                                    FitnessResults.Show()
                                    Me.Hide()
                                End While
                            End If
                        End Using
                    End If
                End Using
                SQLConn.Close()
            End Using
        End If
    End Sub

    Private Sub PreviousEvaluations_SelectedIndexChanged(sender As Object, e As EventArgs) Handles PreviousEvaluations.SelectedIndexChanged
        Dim EvalInfo(2) As String
        ' Split the information from the dropdown box to allow querying of the table
        ' The information in the dropdown consists of two fields in the database
        EvalInfo = PreviousEvaluations.SelectedItem.ToString().Split(",")
        Using SQLConn As New SQLiteConnection(OpeningForm.connection)
            SQLConn.Open()
            Dim selectCommand = SQLConn.CreateCommand()
            selectCommand.CommandText = "SELECT * FROM ClientData WHERE assessment_date = '" & Trim(EvalInfo(1)) & "' AND client_id = " & Me.ClientID.ToString()
            Using reader = selectCommand.ExecuteReader()
                If reader.HasRows() Then
                    While (reader.Read())
                        ' SQLite doesn't allow storing of dates in a date value, so we have to store
                        ' it as string, the next line basically converts it to a form which allows the 
                        ' control to deal with it
                        AssessmentDate.Value = New DateTime(reader.GetString(2).Split(" ")(0).Split("/")(0),
                                                            reader.GetString(2).Split(" ")(0).Split("/")(1),
                                                            reader.GetString(2).Split(" ")(0).Split("/")(2),
                                                            reader.GetString(2).Split(" ")(1).Split(":")(0),
                                                            reader.GetString(2).Split(" ")(1).Split(":")(1),
                                                            reader.GetString(2).Split(" ")(1).Split(":")(2))
                        HeightText.Text = reader.GetDouble(3)
                        WeightText.Text = reader.GetDouble(4)
                        RestingHRText.Text = reader.GetDouble(6)
                        ExerciseHRText.Text = reader.GetDouble(7)
                        RecoverHRText.Text = reader.GetDouble(8)
                        LungFuncText.Text = reader.GetDouble(9)
                        SystolicText.Text = reader.GetDouble(10)
                        DiastolicText.Text = reader.GetDouble(11)
                        WaistText.Text = reader.GetDouble(12)
                    End While
                Else
                    ' Can't retrieve the data, potential for malicious activity
                    ' or alternatively an error with the database not adding stuff
                    MsgBox("There was an error retrieving that data", MsgBoxStyle.Critical, "Error Retrieving")
                End If
            End Using
            SQLConn.Close()
        End Using
    End Sub

    Public Overloads Sub Show(Optional ByVal PassedClientID As Integer = 0)
        ' This takes care of the back button from the next form.
        ' Basically reloading the information to include the new DB rows we 
        ' inserted to get the next form
        If PassedClientID > 0 Then Me.ClientID = PassedClientID
        ReloadInformation()
        MyBase.Show()
    End Sub

    Public Sub ReloadInformation()
        ' Set up the assessment control to show time and disable editing of it
        AssessmentDate.Format = DateTimePickerFormat.Custom
        AssessmentDate.CustomFormat = "dd/mm/yyyy hh:mm:ss"
        AssessmentDate.Enabled = False
        If Me.ClientID > 0 Then
            Using SQLConn As New SQLiteConnection(OpeningForm.connection)
                SQLConn.Open()
                Dim selectCommand = SQLConn.CreateCommand()
                ' Select the necessary fields to populate the dropdown box
                selectCommand.CommandText = "SELECT a.assessment_date, b.name FROM ClientData a, Clients b WHERE a.client_id = " & Me.ClientID.ToString() & " AND b.client_id = " & Me.ClientID.ToString()
                Using reader = selectCommand.ExecuteReader()
                    If reader.HasRows() Then
                        While (reader.Read())
                            ' Add the items to the dropdown box
                            PreviousEvaluations.Items.Add(reader.GetString(1) & ", " & reader.GetString(0))
                        End While
                    End If
                End Using
                SQLConn.Close()
            End Using
        End If
    End Sub

    Private Sub FitnessEvaluation_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
    End Sub
End Class