﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class ClientInformation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.BirthDateLabel = New System.Windows.Forms.Label()
        Me.JobLabel = New System.Windows.Forms.Label()
        Me.BirthDatePicker = New System.Windows.Forms.DateTimePicker()
        Me.Outcome1Text = New System.Windows.Forms.TextBox()
        Me.Outcome3Text = New System.Windows.Forms.TextBox()
        Me.Outcome2Text = New System.Windows.Forms.TextBox()
        Me.Outcome3Label = New System.Windows.Forms.Label()
        Me.Outcome2Label = New System.Windows.Forms.Label()
        Me.Outcome1Label = New System.Windows.Forms.Label()
        Me.JobText = New System.Windows.Forms.TextBox()
        Me.LastNameText = New System.Windows.Forms.TextBox()
        Me.FirstNameText = New System.Windows.Forms.TextBox()
        Me.ClientIDText = New System.Windows.Forms.TextBox()
        Me.LastNameLabel = New System.Windows.Forms.Label()
        Me.FirstNameLabel = New System.Windows.Forms.Label()
        Me.ClientIDLabel = New System.Windows.Forms.Label()
        Me.NextButton = New System.Windows.Forms.Button()
        Me.MaleRadio = New System.Windows.Forms.RadioButton()
        Me.FemaleRadio = New System.Windows.Forms.RadioButton()
        Me.GenderLabel = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.PersonalOutcomeGroup = New System.Windows.Forms.GroupBox()
        Me.ClientInformationGroup = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel1.SuspendLayout()
        Me.PersonalOutcomeGroup.SuspendLayout()
        Me.ClientInformationGroup.SuspendLayout()
        Me.SuspendLayout()
        '
        'BirthDateLabel
        '
        Me.BirthDateLabel.AutoSize = True
        Me.BirthDateLabel.Location = New System.Drawing.Point(246, 51)
        Me.BirthDateLabel.Name = "BirthDateLabel"
        Me.BirthDateLabel.Size = New System.Drawing.Size(54, 13)
        Me.BirthDateLabel.TabIndex = 86
        Me.BirthDateLabel.Text = "Birth Date"
        '
        'JobLabel
        '
        Me.JobLabel.AutoSize = True
        Me.JobLabel.Location = New System.Drawing.Point(276, 26)
        Me.JobLabel.Name = "JobLabel"
        Me.JobLabel.Size = New System.Drawing.Size(24, 13)
        Me.JobLabel.TabIndex = 85
        Me.JobLabel.Text = "Job"
        '
        'BirthDatePicker
        '
        Me.BirthDatePicker.Location = New System.Drawing.Point(306, 48)
        Me.BirthDatePicker.Name = "BirthDatePicker"
        Me.BirthDatePicker.Size = New System.Drawing.Size(239, 20)
        Me.BirthDatePicker.TabIndex = 5
        '
        'Outcome1Text
        '
        Me.Outcome1Text.Location = New System.Drawing.Point(76, 23)
        Me.Outcome1Text.Name = "Outcome1Text"
        Me.Outcome1Text.Size = New System.Drawing.Size(469, 20)
        Me.Outcome1Text.TabIndex = 8
        '
        'Outcome3Text
        '
        Me.Outcome3Text.Location = New System.Drawing.Point(76, 75)
        Me.Outcome3Text.Name = "Outcome3Text"
        Me.Outcome3Text.Size = New System.Drawing.Size(469, 20)
        Me.Outcome3Text.TabIndex = 10
        '
        'Outcome2Text
        '
        Me.Outcome2Text.Location = New System.Drawing.Point(76, 49)
        Me.Outcome2Text.Name = "Outcome2Text"
        Me.Outcome2Text.Size = New System.Drawing.Size(469, 20)
        Me.Outcome2Text.TabIndex = 9
        '
        'Outcome3Label
        '
        Me.Outcome3Label.AutoSize = True
        Me.Outcome3Label.Location = New System.Drawing.Point(9, 78)
        Me.Outcome3Label.Name = "Outcome3Label"
        Me.Outcome3Label.Size = New System.Drawing.Size(59, 13)
        Me.Outcome3Label.TabIndex = 80
        Me.Outcome3Label.Text = "Outcome 3"
        '
        'Outcome2Label
        '
        Me.Outcome2Label.AutoSize = True
        Me.Outcome2Label.Location = New System.Drawing.Point(9, 52)
        Me.Outcome2Label.Name = "Outcome2Label"
        Me.Outcome2Label.Size = New System.Drawing.Size(59, 13)
        Me.Outcome2Label.TabIndex = 79
        Me.Outcome2Label.Text = "Outcome 2"
        '
        'Outcome1Label
        '
        Me.Outcome1Label.AutoSize = True
        Me.Outcome1Label.Location = New System.Drawing.Point(9, 26)
        Me.Outcome1Label.Name = "Outcome1Label"
        Me.Outcome1Label.Size = New System.Drawing.Size(59, 13)
        Me.Outcome1Label.TabIndex = 78
        Me.Outcome1Label.Text = "Outcome 1"
        '
        'JobText
        '
        Me.JobText.Location = New System.Drawing.Point(306, 23)
        Me.JobText.Name = "JobText"
        Me.JobText.Size = New System.Drawing.Size(239, 20)
        Me.JobText.TabIndex = 4
        '
        'LastNameText
        '
        Me.LastNameText.Location = New System.Drawing.Point(76, 74)
        Me.LastNameText.Name = "LastNameText"
        Me.LastNameText.Size = New System.Drawing.Size(129, 20)
        Me.LastNameText.TabIndex = 3
        '
        'FirstNameText
        '
        Me.FirstNameText.Location = New System.Drawing.Point(76, 48)
        Me.FirstNameText.Name = "FirstNameText"
        Me.FirstNameText.Size = New System.Drawing.Size(129, 20)
        Me.FirstNameText.TabIndex = 2
        '
        'ClientIDText
        '
        Me.ClientIDText.Location = New System.Drawing.Point(76, 23)
        Me.ClientIDText.Name = "ClientIDText"
        Me.ClientIDText.Size = New System.Drawing.Size(129, 20)
        Me.ClientIDText.TabIndex = 1
        '
        'LastNameLabel
        '
        Me.LastNameLabel.AutoSize = True
        Me.LastNameLabel.Location = New System.Drawing.Point(10, 77)
        Me.LastNameLabel.Name = "LastNameLabel"
        Me.LastNameLabel.Size = New System.Drawing.Size(58, 13)
        Me.LastNameLabel.TabIndex = 72
        Me.LastNameLabel.Text = "Last Name"
        '
        'FirstNameLabel
        '
        Me.FirstNameLabel.AutoSize = True
        Me.FirstNameLabel.Location = New System.Drawing.Point(11, 51)
        Me.FirstNameLabel.Name = "FirstNameLabel"
        Me.FirstNameLabel.Size = New System.Drawing.Size(57, 13)
        Me.FirstNameLabel.TabIndex = 71
        Me.FirstNameLabel.Text = "First Name"
        '
        'ClientIDLabel
        '
        Me.ClientIDLabel.AutoSize = True
        Me.ClientIDLabel.Location = New System.Drawing.Point(21, 26)
        Me.ClientIDLabel.Name = "ClientIDLabel"
        Me.ClientIDLabel.Size = New System.Drawing.Size(47, 13)
        Me.ClientIDLabel.TabIndex = 70
        Me.ClientIDLabel.Text = "Client ID"
        '
        'NextButton
        '
        Me.NextButton.Location = New System.Drawing.Point(500, 264)
        Me.NextButton.Name = "NextButton"
        Me.NextButton.Size = New System.Drawing.Size(75, 23)
        Me.NextButton.TabIndex = 11
        Me.NextButton.Text = "Next"
        Me.NextButton.UseVisualStyleBackColor = True
        '
        'MaleRadio
        '
        Me.MaleRadio.AutoSize = True
        Me.MaleRadio.Location = New System.Drawing.Point(3, 3)
        Me.MaleRadio.Name = "MaleRadio"
        Me.MaleRadio.Size = New System.Drawing.Size(48, 17)
        Me.MaleRadio.TabIndex = 6
        Me.MaleRadio.TabStop = True
        Me.MaleRadio.Text = "Male"
        Me.MaleRadio.UseVisualStyleBackColor = True
        '
        'FemaleRadio
        '
        Me.FemaleRadio.AutoSize = True
        Me.FemaleRadio.Location = New System.Drawing.Point(57, 3)
        Me.FemaleRadio.Name = "FemaleRadio"
        Me.FemaleRadio.Size = New System.Drawing.Size(59, 17)
        Me.FemaleRadio.TabIndex = 7
        Me.FemaleRadio.TabStop = True
        Me.FemaleRadio.Text = "Female"
        Me.FemaleRadio.UseVisualStyleBackColor = True
        '
        'GenderLabel
        '
        Me.GenderLabel.AutoSize = True
        Me.GenderLabel.Location = New System.Drawing.Point(258, 79)
        Me.GenderLabel.Name = "GenderLabel"
        Me.GenderLabel.Size = New System.Drawing.Size(42, 13)
        Me.GenderLabel.TabIndex = 98
        Me.GenderLabel.Text = "Gender"
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.Controls.Add(Me.MaleRadio)
        Me.FlowLayoutPanel1.Controls.Add(Me.FemaleRadio)
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(306, 74)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(122, 24)
        Me.FlowLayoutPanel1.TabIndex = 99
        '
        'PersonalOutcomeGroup
        '
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome1Label)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome2Label)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome3Label)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome1Text)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome2Text)
        Me.PersonalOutcomeGroup.Controls.Add(Me.Outcome3Text)
        Me.PersonalOutcomeGroup.Location = New System.Drawing.Point(12, 145)
        Me.PersonalOutcomeGroup.Name = "PersonalOutcomeGroup"
        Me.PersonalOutcomeGroup.Size = New System.Drawing.Size(563, 113)
        Me.PersonalOutcomeGroup.TabIndex = 100
        Me.PersonalOutcomeGroup.TabStop = False
        Me.PersonalOutcomeGroup.Text = "Personal Outcomes"
        '
        'ClientInformationGroup
        '
        Me.ClientInformationGroup.Controls.Add(Me.ClientIDLabel)
        Me.ClientInformationGroup.Controls.Add(Me.FirstNameLabel)
        Me.ClientInformationGroup.Controls.Add(Me.FlowLayoutPanel1)
        Me.ClientInformationGroup.Controls.Add(Me.LastNameLabel)
        Me.ClientInformationGroup.Controls.Add(Me.GenderLabel)
        Me.ClientInformationGroup.Controls.Add(Me.ClientIDText)
        Me.ClientInformationGroup.Controls.Add(Me.FirstNameText)
        Me.ClientInformationGroup.Controls.Add(Me.BirthDateLabel)
        Me.ClientInformationGroup.Controls.Add(Me.LastNameText)
        Me.ClientInformationGroup.Controls.Add(Me.JobLabel)
        Me.ClientInformationGroup.Controls.Add(Me.JobText)
        Me.ClientInformationGroup.Controls.Add(Me.BirthDatePicker)
        Me.ClientInformationGroup.Location = New System.Drawing.Point(12, 12)
        Me.ClientInformationGroup.Name = "ClientInformationGroup"
        Me.ClientInformationGroup.Size = New System.Drawing.Size(563, 117)
        Me.ClientInformationGroup.TabIndex = 101
        Me.ClientInformationGroup.TabStop = False
        Me.ClientInformationGroup.Text = "Client Information"
        '
        'ClientInformation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PowderBlue
        Me.ClientSize = New System.Drawing.Size(586, 293)
        Me.Controls.Add(Me.ClientInformationGroup)
        Me.Controls.Add(Me.PersonalOutcomeGroup)
        Me.Controls.Add(Me.NextButton)
        Me.Name = "ClientInformation"
        Me.Text = "Client Personal Details"
        Me.FlowLayoutPanel1.ResumeLayout(False)
        Me.FlowLayoutPanel1.PerformLayout()
        Me.PersonalOutcomeGroup.ResumeLayout(False)
        Me.PersonalOutcomeGroup.PerformLayout()
        Me.ClientInformationGroup.ResumeLayout(False)
        Me.ClientInformationGroup.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents BirthDateLabel As Label
    Friend WithEvents JobLabel As Label
    Friend WithEvents BirthDatePicker As DateTimePicker
    Friend WithEvents Outcome1Text As TextBox
    Friend WithEvents Outcome3Text As TextBox
    Friend WithEvents Outcome2Text As TextBox
    Friend WithEvents Outcome3Label As Label
    Friend WithEvents Outcome2Label As Label
    Friend WithEvents Outcome1Label As Label
    Friend WithEvents JobText As TextBox
    Friend WithEvents LastNameText As TextBox
    Friend WithEvents FirstNameText As TextBox
    Friend WithEvents ClientIDText As TextBox
    Friend WithEvents LastNameLabel As Label
    Friend WithEvents FirstNameLabel As Label
    Friend WithEvents ClientIDLabel As Label
    Friend WithEvents NextButton As Button
    Friend WithEvents MaleRadio As RadioButton
    Friend WithEvents FemaleRadio As RadioButton
    Friend WithEvents GenderLabel As Label
    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents PersonalOutcomeGroup As GroupBox
    Friend WithEvents ClientInformationGroup As GroupBox
End Class
