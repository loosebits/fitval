﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class OpeningForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.Form2ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Form2ToolStripMenuItem1 = New System.Windows.Forms.ToolStripMenuItem()
        Me.Login = New System.Windows.Forms.Button()
        Me.CreateID = New System.Windows.Forms.Button()
        Me.EnterID = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.ContextMenuStrip2.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.Form2ToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(109, 26)
        '
        'Form2ToolStripMenuItem
        '
        Me.Form2ToolStripMenuItem.Name = "Form2ToolStripMenuItem"
        Me.Form2ToolStripMenuItem.Size = New System.Drawing.Size(108, 22)
        Me.Form2ToolStripMenuItem.Text = "Form2"
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripMenuItem1, Me.Form2ToolStripMenuItem1})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(184, 48)
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(183, 22)
        Me.ToolStripMenuItem1.Text = "ToolStripMenuItem1"
        '
        'Form2ToolStripMenuItem1
        '
        Me.Form2ToolStripMenuItem1.Name = "Form2ToolStripMenuItem1"
        Me.Form2ToolStripMenuItem1.Size = New System.Drawing.Size(183, 22)
        Me.Form2ToolStripMenuItem1.Text = "Form2"
        '
        'Login
        '
        Me.Login.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.Login.Location = New System.Drawing.Point(64, 109)
        Me.Login.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Login.Name = "Login"
        Me.Login.Size = New System.Drawing.Size(147, 30)
        Me.Login.TabIndex = 2
        Me.Login.Text = "Continue"
        Me.Login.UseVisualStyleBackColor = False
        '
        'CreateID
        '
        Me.CreateID.BackColor = System.Drawing.SystemColors.ButtonFace
        Me.CreateID.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CreateID.Location = New System.Drawing.Point(76, 225)
        Me.CreateID.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.CreateID.Name = "CreateID"
        Me.CreateID.Size = New System.Drawing.Size(119, 25)
        Me.CreateID.TabIndex = 3
        Me.CreateID.Text = "Create Client ID"
        Me.CreateID.UseVisualStyleBackColor = False
        '
        'EnterID
        '
        Me.EnterID.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.EnterID.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me.EnterID.Location = New System.Drawing.Point(64, 64)
        Me.EnterID.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.EnterID.Name = "EnterID"
        Me.EnterID.Size = New System.Drawing.Size(147, 21)
        Me.EnterID.TabIndex = 1
        Me.EnterID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(85, 32)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 18)
        Me.Label1.TabIndex = 6
        Me.Label1.Text = "Enter Client ID"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(73, 197)
        Me.Label2.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(123, 15)
        Me.Label2.TabIndex = 8
        Me.Label2.Text = "If you dont have an ID"
        '
        'OpeningForm
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PowderBlue
        Me.ClientSize = New System.Drawing.Size(291, 303)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.EnterID)
        Me.Controls.Add(Me.CreateID)
        Me.Controls.Add(Me.Login)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Name = "OpeningForm"
        Me.Text = "FitVal - Login"
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.ContextMenuStrip2.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents ContextMenuStrip1 As ContextMenuStrip
    Friend WithEvents Form2ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents ContextMenuStrip2 As ContextMenuStrip
    Friend WithEvents ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents Form2ToolStripMenuItem1 As ToolStripMenuItem
    Friend WithEvents Login As Button
    Friend WithEvents CreateID As Button
    Friend WithEvents EnterID As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
