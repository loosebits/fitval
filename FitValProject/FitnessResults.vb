﻿Imports System.Data.SQLite

Public Class FitnessResults
    Dim ClientID As Integer
    Dim RAGStatus As New Dictionary(Of Color, Integer)

    Public Sub New(PassFitnessEvaluation As Integer)
        InitializeComponent()
        ' Add the colours to the rag status, to track how many of each
        RAGStatus.Add(Color.Red, 0)
        RAGStatus.Add(Color.Yellow, 0)
        RAGStatus.Add(Color.Green, 0)
        Using SQLConn As New SQLiteConnection(OpeningForm.connection)
            SQLConn.Open()
            Dim selectCommand = SQLConn.CreateCommand()
            ' Join the information from both client and assessment with SQL
            selectCommand.CommandText = "SELECT b.*, a.* FROM ClientData a, Clients b WHERE a.data_id = '" & PassFitnessEvaluation.ToString() & "' AND a.client_id = b.client_id"
            Using reader = selectCommand.ExecuteReader()
                If reader.HasRows() Then
                    While (reader.Read())
                        ' The next lines basically populate all the form elements
                        ClientIDText.Text = reader.GetInt32(0)
                        Me.ClientID = reader.GetInt32(0)
                        ClientIDText.Enabled = False
                        BirthDatePicker.Value = New Date(reader.GetString(2).Split("/")(2), reader.GetString(2).Split("/")(1), reader.GetString(2).Split("/")(0))

                        JobText.Text = reader.GetString(6)
                        FirstNameText.Text = reader.GetString(7)
                        LastNameText.Text = reader.GetString(8)
                        If reader.GetInt32(9) = 2 Then
                            FemaleRadio.Checked = True
                        Else
                            MaleRadio.Checked = True
                        End If
                        Outcome1Text.Text = reader.GetString(10)
                        Outcome2Text.Text = reader.GetString(11)
                        Outcome3Text.Text = reader.GetString(12)

                        AssessmentDatePicker.Format = DateTimePickerFormat.Custom
                        AssessmentDatePicker.CustomFormat = "dd/mm/yyyy hh:mm:ss"
                        AssessmentDatePicker.Enabled = False

                        AssessmentDatePicker.Value = New DateTime(reader.GetString(15).Split(" ")(0).Split("/")(0),
                                                                  reader.GetString(15).Split(" ")(0).Split("/")(1),
                                                                  reader.GetString(15).Split(" ")(0).Split("/")(2),
                                                                  reader.GetString(15).Split(" ")(1).Split(":")(0),
                                                                  reader.GetString(15).Split(" ")(1).Split(":")(1),
                                                                  reader.GetString(15).Split(" ")(1).Split(":")(2))

                        HeightText.Text = reader.GetDouble(16)

                        If FemaleRadio.Checked = True Then ChangeColour(WeightTextR, 60.0, 70.0, reader.GetDouble(17))
                        If MaleRadio.Checked = True Then ChangeColour(WeightTextR, 63.0, 86.0, reader.GetDouble(17))

                        If FemaleRadio.Checked = True Then ChangeColour(BMITextR, 25.0, 31.0, reader.GetDouble(18))
                        If MaleRadio.Checked = True Then ChangeColour(BMITextR, 25.0, 31.0, reader.GetDouble(18))

                        If FemaleRadio.Checked = True Then ChangeColour(RestingHRTextR, 65.0, 78.0, reader.GetDouble(19))
                        If MaleRadio.Checked = True Then ChangeColour(RestingHRTextR, 67.0, 75.0, reader.GetDouble(19))

                        If FemaleRadio.Checked = True Then ChangeColour(ExerciseHRTextR, 162.0, 190.0, reader.GetDouble(20))
                        If MaleRadio.Checked = True Then ChangeColour(ExerciseHRTextR, 147.0, 171.0, reader.GetDouble(20))

                        If FemaleRadio.Checked = True Then ChangeColour(RecoveryHRTextR, 95.0, 110.0, reader.GetDouble(21))
                        If MaleRadio.Checked = True Then ChangeColour(RecoveryHRTextR, 100.0, 130.0, reader.GetDouble(21))

                        If FemaleRadio.Checked = True Then ChangeColour(LungFuncTextR, 222.5, 445.0, reader.GetDouble(22))
                        If MaleRadio.Checked = True Then ChangeColour(LungFuncTextR, 325.0, 650.0, reader.GetDouble(22))

                        ChangeColour(SystolicTextR, 120.0, 140.0, reader.GetDouble(23))

                        If FemaleRadio.Checked = True Then ChangeColour(DiastolicTextR, 80.0, 90.0, reader.GetDouble(24))
                        If MaleRadio.Checked = True Then ChangeColour(DiastolicTextR, 79.0, 90.0, reader.GetDouble(24))

                        If FemaleRadio.Checked = True Then ChangeColour(WaistTextR, 79.0, 88.0, reader.GetDouble(25))
                        If MaleRadio.Checked = True Then ChangeColour(WaistTextR, 96.0, 104.0, reader.GetDouble(25))

                    End While
                End If
            End Using
            SQLConn.Close()
        End Using
        ' Sorting function from Sam Axe on Stack Overflow
        ' http://stackoverflow.com/a/14165941
        Dim SortedRAG = (From item In RAGStatus Order By item.Value Select item).ToList
        OverallFitnessText.BackColor = SortedRAG.Item(2).Key
    End Sub

    Private Sub ChangeColour(TextBoxObject As TextBox, Lower As Double, Upper As Double, Actual As Object)
        Dim GenericName As String
        Dim RAGTextBox As TextBox
        Dim NewColor As Color
        ' Basically grabbing the coloured name from the TextBox object as it is a common prefix
        GenericName = TextBoxObject.Name.Substring(0, TextBoxObject.Name.Length - 1)
        ' Sort out it's corresponding colour
        If Actual <= Lower Then NewColor = Color.Green
        If Actual > Lower And Actual <= Upper Then NewColor = Color.Yellow
        If Actual > Upper Then NewColor = Color.Red

        If Me.Controls.Find(GenericName & NewColor.Name.Substring(0, 1), True).Length = 1 Then
            ' Increment the count of colours, to show the overall fitness
            RAGStatus.Item(NewColor) += 1
            ' Grab the first result of the find of controls which exist for that colour
            RAGTextBox = Me.Controls.Find(GenericName & NewColor.Name.Substring(0, 1), True)(0)
            RAGTextBox.BackColor = NewColor
            ' Change the text to white if the back is red
            If RAGTextBox.BackColor = Color.Red Then RAGTextBox.ForeColor = Color.White
            RAGTextBox.Text = Actual.ToString()
            RAGTextBox.TextAlign = HorizontalAlignment.Center
        End If
    End Sub

    Private Sub BackButton_Click(sender As Object, e As EventArgs) Handles BackButton.Click
        Dim NewForm As New FitnessEvaluation()
        NewForm.Show(Me.ClientID)
        Me.Hide()
    End Sub

    Private Sub FitnessResults_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Me.CenterToScreen()
    End Sub

    Private Sub Print_Click(sender As Object, e As EventArgs) Handles Print.Click
        If PrintDialog1.ShowDialog = DialogResult.OK Then
            PrintForm1.PrinterSettings = PrintDialog1.PrinterSettings
        End If

        PrintForm1.Print(Me, PowerPacks.Printing.PrintForm.PrintOption.Scrollable)
    End Sub
End Class

