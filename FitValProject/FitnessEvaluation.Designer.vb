﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class FitnessEvaluation
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.HeightText = New System.Windows.Forms.TextBox()
        Me.ViewResults = New System.Windows.Forms.Button()
        Me.WeightText = New System.Windows.Forms.TextBox()
        Me.RestingHRText = New System.Windows.Forms.TextBox()
        Me.ExerciseHRText = New System.Windows.Forms.TextBox()
        Me.RecoverHRText = New System.Windows.Forms.TextBox()
        Me.LungFuncText = New System.Windows.Forms.TextBox()
        Me.SystolicText = New System.Windows.Forms.TextBox()
        Me.DiastolicText = New System.Windows.Forms.TextBox()
        Me.WaistText = New System.Windows.Forms.TextBox()
        Me.HeightLabel = New System.Windows.Forms.Label()
        Me.WeightLabel = New System.Windows.Forms.Label()
        Me.RestingHRLabel = New System.Windows.Forms.Label()
        Me.ExerciseHRLabel = New System.Windows.Forms.Label()
        Me.RecoverHRLabel = New System.Windows.Forms.Label()
        Me.LungFuncLabel = New System.Windows.Forms.Label()
        Me.SystolicLabel = New System.Windows.Forms.Label()
        Me.DiastolicLabel = New System.Windows.Forms.Label()
        Me.WaistLabel = New System.Windows.Forms.Label()
        Me.PreviousEvaluations = New System.Windows.Forms.ComboBox()
        Me.AssessmentDate = New System.Windows.Forms.DateTimePicker()
        Me.SuspendLayout()
        '
        'HeightText
        '
        Me.HeightText.Location = New System.Drawing.Point(211, 65)
        Me.HeightText.Name = "HeightText"
        Me.HeightText.Size = New System.Drawing.Size(100, 20)
        Me.HeightText.TabIndex = 3
        '
        'ViewResults
        '
        Me.ViewResults.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ViewResults.Location = New System.Drawing.Point(135, 309)
        Me.ViewResults.Name = "ViewResults"
        Me.ViewResults.Size = New System.Drawing.Size(97, 27)
        Me.ViewResults.TabIndex = 12
        Me.ViewResults.Text = "View Results"
        Me.ViewResults.UseVisualStyleBackColor = True
        '
        'WeightText
        '
        Me.WeightText.Location = New System.Drawing.Point(211, 91)
        Me.WeightText.Name = "WeightText"
        Me.WeightText.Size = New System.Drawing.Size(100, 20)
        Me.WeightText.TabIndex = 4
        '
        'RestingHRText
        '
        Me.RestingHRText.Location = New System.Drawing.Point(211, 117)
        Me.RestingHRText.Name = "RestingHRText"
        Me.RestingHRText.Size = New System.Drawing.Size(100, 20)
        Me.RestingHRText.TabIndex = 5
        '
        'ExerciseHRText
        '
        Me.ExerciseHRText.Location = New System.Drawing.Point(211, 143)
        Me.ExerciseHRText.Name = "ExerciseHRText"
        Me.ExerciseHRText.Size = New System.Drawing.Size(100, 20)
        Me.ExerciseHRText.TabIndex = 6
        '
        'RecoverHRText
        '
        Me.RecoverHRText.Location = New System.Drawing.Point(211, 169)
        Me.RecoverHRText.Name = "RecoverHRText"
        Me.RecoverHRText.Size = New System.Drawing.Size(100, 20)
        Me.RecoverHRText.TabIndex = 7
        '
        'LungFuncText
        '
        Me.LungFuncText.Location = New System.Drawing.Point(211, 195)
        Me.LungFuncText.Name = "LungFuncText"
        Me.LungFuncText.Size = New System.Drawing.Size(100, 20)
        Me.LungFuncText.TabIndex = 8
        '
        'SystolicText
        '
        Me.SystolicText.Location = New System.Drawing.Point(211, 221)
        Me.SystolicText.Name = "SystolicText"
        Me.SystolicText.Size = New System.Drawing.Size(100, 20)
        Me.SystolicText.TabIndex = 9
        '
        'DiastolicText
        '
        Me.DiastolicText.Location = New System.Drawing.Point(211, 247)
        Me.DiastolicText.Name = "DiastolicText"
        Me.DiastolicText.Size = New System.Drawing.Size(100, 20)
        Me.DiastolicText.TabIndex = 10
        '
        'WaistText
        '
        Me.WaistText.Location = New System.Drawing.Point(211, 273)
        Me.WaistText.Name = "WaistText"
        Me.WaistText.Size = New System.Drawing.Size(100, 20)
        Me.WaistText.TabIndex = 11
        '
        'HeightLabel
        '
        Me.HeightLabel.AutoSize = True
        Me.HeightLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.HeightLabel.Location = New System.Drawing.Point(132, 68)
        Me.HeightLabel.Name = "HeightLabel"
        Me.HeightLabel.Size = New System.Drawing.Size(72, 13)
        Me.HeightLabel.TabIndex = 21
        Me.HeightLabel.Text = "Height (cm)"
        '
        'WeightLabel
        '
        Me.WeightLabel.AutoSize = True
        Me.WeightLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WeightLabel.Location = New System.Drawing.Point(131, 94)
        Me.WeightLabel.Name = "WeightLabel"
        Me.WeightLabel.Size = New System.Drawing.Size(73, 13)
        Me.WeightLabel.TabIndex = 22
        Me.WeightLabel.Text = "Weight (kg)"
        '
        'RestingHRLabel
        '
        Me.RestingHRLabel.AutoSize = True
        Me.RestingHRLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RestingHRLabel.Location = New System.Drawing.Point(60, 120)
        Me.RestingHRLabel.Name = "RestingHRLabel"
        Me.RestingHRLabel.Size = New System.Drawing.Size(144, 13)
        Me.RestingHRLabel.TabIndex = 23
        Me.RestingHRLabel.Text = "Resting heart rate (bpm)"
        '
        'ExerciseHRLabel
        '
        Me.ExerciseHRLabel.AutoSize = True
        Me.ExerciseHRLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ExerciseHRLabel.Location = New System.Drawing.Point(55, 146)
        Me.ExerciseHRLabel.Name = "ExerciseHRLabel"
        Me.ExerciseHRLabel.Size = New System.Drawing.Size(149, 13)
        Me.ExerciseHRLabel.TabIndex = 24
        Me.ExerciseHRLabel.Text = "Exercise heart rate (bpm)"
        '
        'RecoverHRLabel
        '
        Me.RecoverHRLabel.AutoSize = True
        Me.RecoverHRLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RecoverHRLabel.Location = New System.Drawing.Point(55, 172)
        Me.RecoverHRLabel.Name = "RecoverHRLabel"
        Me.RecoverHRLabel.Size = New System.Drawing.Size(149, 13)
        Me.RecoverHRLabel.TabIndex = 25
        Me.RecoverHRLabel.Text = "Recover heart rate (bpm)"
        '
        'LungFuncLabel
        '
        Me.LungFuncLabel.AutoSize = True
        Me.LungFuncLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.LungFuncLabel.Location = New System.Drawing.Point(79, 198)
        Me.LungFuncLabel.Name = "LungFuncLabel"
        Me.LungFuncLabel.Size = New System.Drawing.Size(125, 13)
        Me.LungFuncLabel.TabIndex = 26
        Me.LungFuncLabel.Text = "Lung function (l/min)"
        '
        'SystolicLabel
        '
        Me.SystolicLabel.AutoSize = True
        Me.SystolicLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.SystolicLabel.Location = New System.Drawing.Point(18, 224)
        Me.SystolicLabel.Name = "SystolicLabel"
        Me.SystolicLabel.Size = New System.Drawing.Size(186, 13)
        Me.SystolicLabel.TabIndex = 27
        Me.SystolicLabel.Text = "Systolic blood pressure (mmHG)"
        '
        'DiastolicLabel
        '
        Me.DiastolicLabel.AutoSize = True
        Me.DiastolicLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.DiastolicLabel.Location = New System.Drawing.Point(13, 250)
        Me.DiastolicLabel.Name = "DiastolicLabel"
        Me.DiastolicLabel.Size = New System.Drawing.Size(191, 13)
        Me.DiastolicLabel.TabIndex = 28
        Me.DiastolicLabel.Text = "Diastolic blood pressure (mmHG)"
        '
        'WaistLabel
        '
        Me.WaistLabel.AutoSize = True
        Me.WaistLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.WaistLabel.Location = New System.Drawing.Point(59, 276)
        Me.WaistLabel.Name = "WaistLabel"
        Me.WaistLabel.Size = New System.Drawing.Size(145, 13)
        Me.WaistLabel.TabIndex = 29
        Me.WaistLabel.Text = "Waist measurement (cm)"
        '
        'PreviousEvaluations
        '
        Me.PreviousEvaluations.FormattingEnabled = True
        Me.PreviousEvaluations.Location = New System.Drawing.Point(13, 13)
        Me.PreviousEvaluations.Name = "PreviousEvaluations"
        Me.PreviousEvaluations.Size = New System.Drawing.Size(298, 21)
        Me.PreviousEvaluations.TabIndex = 1
        '
        'AssessmentDate
        '
        Me.AssessmentDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.AssessmentDate.Location = New System.Drawing.Point(110, 39)
        Me.AssessmentDate.Name = "AssessmentDate"
        Me.AssessmentDate.Size = New System.Drawing.Size(200, 20)
        Me.AssessmentDate.TabIndex = 2
        '
        'FitnessEvaluation
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.PowderBlue
        Me.ClientSize = New System.Drawing.Size(346, 349)
        Me.Controls.Add(Me.AssessmentDate)
        Me.Controls.Add(Me.PreviousEvaluations)
        Me.Controls.Add(Me.WaistLabel)
        Me.Controls.Add(Me.DiastolicLabel)
        Me.Controls.Add(Me.SystolicLabel)
        Me.Controls.Add(Me.LungFuncLabel)
        Me.Controls.Add(Me.RecoverHRLabel)
        Me.Controls.Add(Me.ExerciseHRLabel)
        Me.Controls.Add(Me.RestingHRLabel)
        Me.Controls.Add(Me.WeightLabel)
        Me.Controls.Add(Me.HeightLabel)
        Me.Controls.Add(Me.WaistText)
        Me.Controls.Add(Me.DiastolicText)
        Me.Controls.Add(Me.SystolicText)
        Me.Controls.Add(Me.LungFuncText)
        Me.Controls.Add(Me.RecoverHRText)
        Me.Controls.Add(Me.ExerciseHRText)
        Me.Controls.Add(Me.RestingHRText)
        Me.Controls.Add(Me.WeightText)
        Me.Controls.Add(Me.ViewResults)
        Me.Controls.Add(Me.HeightText)
        Me.Name = "FitnessEvaluation"
        Me.Text = "Fitness Evaluation - Test Data"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents HeightText As TextBox
    Friend WithEvents ViewResults As Button
    Friend WithEvents WeightText As TextBox
    Friend WithEvents RestingHRText As TextBox
    Friend WithEvents ExerciseHRText As TextBox
    Friend WithEvents RecoverHRText As TextBox
    Friend WithEvents LungFuncText As TextBox
    Friend WithEvents SystolicText As TextBox
    Friend WithEvents DiastolicText As TextBox
    Friend WithEvents WaistText As TextBox
    Friend WithEvents HeightLabel As Label
    Friend WithEvents WeightLabel As Label
    Friend WithEvents RestingHRLabel As Label
    Friend WithEvents ExerciseHRLabel As Label
    Friend WithEvents RecoverHRLabel As Label
    Friend WithEvents LungFuncLabel As Label
    Friend WithEvents SystolicLabel As Label
    Friend WithEvents DiastolicLabel As Label
    Friend WithEvents WaistLabel As Label
    Friend WithEvents PreviousEvaluations As ComboBox
    Friend WithEvents AssessmentDate As DateTimePicker
End Class
