﻿' FitVal Version 1.0
' Code written by: Ben Reich
' Initial Design Layout: Carlos Cerna

Imports System.Data.SQLite
Imports System.IO

Public Class OpeningForm
    ' Set up the connection as a string
    Public connection As String = "Data source=.\FitVal.db;Version=3"

    Public Sub New()
        InitializeComponent()
        ' Create the db if it does not already exist (including the DB schema)
        If File.Exists(".\FitVal.db") = False Then
            Dim SQLConn As New SQLiteConnection(Me.connection)
            SQLConn.Open()
            Dim command = SQLConn.CreateCommand()
            ' Execute the SQL command to generate the schema
            command.CommandText = CreateDBCommand()
            command.ExecuteNonQuery()
            SQLConn.Close()
        End If
    End Sub

    Private Sub Login_Click(sender As Object, e As EventArgs) Handles Login.Click
        Dim ClientID As Integer
        Dim createTables As Boolean = False
        ' Simplistic error checking to avoid too many chars
        If EnterID.Text.Length > 0 And EnterID.Text.Length < 30 Then
            ' Error checking for wrong type entered in Client ID
            If Integer.TryParse(EnterID.Text, ClientID) Then
                Using SQLConn As New SQLiteConnection(Me.connection)
                    Try
                        SQLConn.Open()
                        Dim selectCommand = SQLConn.CreateCommand()
                        ' Select the client who's ID pertains to the one entered
                        ' Shouldn't run the risk of wrong data as we did some error checking prior
                        selectCommand.CommandText = "Select client_id FROM Clients WHERE client_id = '" & ClientID & "'"
                        Using reader = selectCommand.ExecuteReader()
                            If reader.HasRows = True Then
                                ' If a client exists, show the client form and pass through the ID
                                Dim ClientForm As New ClientInformation(ClientID)
                                ClientForm.Show()
                                Me.Hide()
                            Else
                                ' If it doesn't exist, just show the client informtion with blank ID
                                Dim ClientForm As New ClientInformation()
                                ClientForm.Show()
                                Me.Hide()
                            End If
                        End Using
                        SQLConn.Close()
                    Catch err As SQLiteException
                        ' We had an error with the opening of the DB, shut down
                        MsgBox("Error opening database, shutting down", MsgBoxStyle.Critical, "Database Error")
                        Application.Exit()
                        End
                    End Try
                End Using
            End If
        Else
            ' Show the client information with a blank ID
            Dim ClientForm As New ClientInformation()
            ClientForm.Show()
            Me.Hide()
        End If

    End Sub

    Private Sub CreateID_Click(sender As Object, e As EventArgs) Handles CreateID.Click
        ' Simple function to take care of the create ID button
        Dim ClientForm As New ClientInformation
        ClientForm.Show()
        Me.Hide()
    End Sub

    Public Function CreateDBCommand()
        Dim createCommand = "
-- Table: ClientData
CREATE TABLE ClientData (
    data_id integer NOT NULL CONSTRAINT ClientData_pk PRIMARY KEY AUTOINCREMENT,
    client_id integer NOT NULL,
    assessment_date text NOT NULL,
    height real NOT NULL,
    weight real NOT NULL,
    bmi real NOT NULL,
    rest_hr real NOT NULL,
    max_hr real NOT NULL,
    rec_time real NOT NULL,
    lung_func real NOT NULL,
    blood_press_sys integer NOT NULL,
    blood_press_dia integer NOT NULL,
    waist real NOT NULL,
    CONSTRAINT ClientsToClientData FOREIGN KEY (client_id)
    REFERENCES Clients (client_id)
    ON DELETE CASCADE 
    ON UPDATE CASCADE
);

-- Table: Clients
CREATE TABLE Clients (
    client_id integer NOT NULL CONSTRAINT Clients_pk PRIMARY KEY AUTOINCREMENT,
    name text NOT NULL,
    birth_date text NOT NULL,
    total_inputs integer NULL,
    first_entered text NULL,
    latest_entered text NULL,
    job text NOT NULL,
    first_name text NOT NULL,
    last_name text NOT NULL,
    gender integer NOT NULL,
    poutcome_1 text NOT NULL,
    poutcome_2 text NOT NULL,
    poutcome_3 text NOT NULL
);
"
        Return createCommand
    End Function

    Private Sub OpeningForm_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        ' Basically onload of the form, center it to the screen
        Me.CenterToScreen()
    End Sub
End Class
